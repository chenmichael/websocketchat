﻿using System;
using System.XConsole;

namespace TestWebsocketApp {
    public class Program {
        private const int WEBSOCKETPORT = 2654;
        static void Main() {
            new Program().Run();
            Console.WriteLine("Press any key to exit.");
            while (Console.KeyAvailable)
                Console.ReadKey(true);
            Console.ReadKey(true);
        }

        private void Run() {
            Console.WriteLine("Client or Server? (C/S)");
            try {
                if (XConsole.UserConfirm(ConsoleKey.C, ConsoleKey.S)) {
                    Console.WriteLine("Please enter your username:");
                    var client = new Client.Client($"ws://localhost:{WEBSOCKETPORT}/chat", Console.ReadLine());
                    client.Run();
                } else {
                    var server = new Server.Server(WEBSOCKETPORT);
                    server.Run();
                }
            } catch (Exception e) {
                XConsole.WriteLine(ConsoleColor.Red, "Uncaught exception: {0}", e.ToString());
            }
        }
    }
}

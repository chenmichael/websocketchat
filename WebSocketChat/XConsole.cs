﻿using System.Diagnostics;

namespace System.XConsole {
    [DebuggerStepThrough]
    public static class XConsole {
        public static void WriteLine(ConsoleColor color, string message, params string[] args) {
            var prev = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.WriteLine(message, args);
            Console.ForegroundColor = prev;
        }
        public static void Write(ConsoleColor color, string message, params string[] args) {
            var prev = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.Write(message, args);
            Console.ForegroundColor = prev;
        }
        public static bool UserConfirm(ConsoleKey truevalue, ConsoleKey falsevalue) {
            while (true) {
                var key = Console.ReadKey(true).Key;
                if (key == truevalue)
                    return true;
                else if (key == falsevalue)
                    return false;
            }
        }
    }
}

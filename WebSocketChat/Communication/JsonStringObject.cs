﻿using Newtonsoft.Json;
using System.Diagnostics;

namespace TestWebsocketApp.Communication {
    public abstract class CommObject {
        [JsonIgnore]
        public abstract MessageType Type { get; }
        [DebuggerStepThrough]
        public override string ToString() => JsonConvert.SerializeObject(this);
    }
}
﻿namespace TestWebsocketApp.Communication {
    public enum MessageType {
        Unknown,
        AuthenticationRequest,
        AuthenticationAnswer,
        TextMessageSend,
        TextMessageReceive,
        WrappedMessage
    }
}
﻿using System.Diagnostics;

namespace TestWebsocketApp.Communication {
    [DebuggerStepThrough]
    public class AuthenticationAnswerData : CommObject {
        public override MessageType Type => MessageType.AuthenticationAnswer;
        public AuthenticationAnswerData() { }
        public AuthenticationAnswerData(bool success, string reason = null) : this() {
            Success = success;
            Reason = reason;
        }

        public bool Success { get; set; }
        public string Reason { get; set; }
    }
}
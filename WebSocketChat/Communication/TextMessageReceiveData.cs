﻿using System.Diagnostics;

namespace TestWebsocketApp.Communication {
    [DebuggerStepThrough]
    public class TextMessageReceiveData : CommObject {
        public override MessageType Type => MessageType.TextMessageReceive;
        public TextMessageReceiveData() { }
        public TextMessageReceiveData(string sender, string message) : this() {
            Sender = sender;
            Message = message;
        }

        public string Sender { get; set; }
        public string Message { get; set; }
    }
}
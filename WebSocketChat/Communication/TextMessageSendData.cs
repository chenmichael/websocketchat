﻿using System.Diagnostics;

namespace TestWebsocketApp.Communication {
    [DebuggerStepThrough]
    public class TextMessageSendData : CommObject {
        public override MessageType Type => MessageType.TextMessageSend;
        public TextMessageSendData() { }
        public TextMessageSendData(string message) : this() {
            Message = message;
        }

        public string Message { get; set; }
    }
}
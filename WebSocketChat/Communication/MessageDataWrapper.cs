﻿using System.Diagnostics;

namespace TestWebsocketApp.Communication {
    [DebuggerStepThrough]
    public class MessageDataWrapper : CommObject {
        public override MessageType Type => MessageType.WrappedMessage;
        public MessageDataWrapper() { }
        public MessageDataWrapper(CommObject payload, MessageType type) : this() {
            Payload = payload.ToString();
            MessageType = type;
        }

        public string Payload { get; set; }
        public MessageType MessageType { get; set; }
    }
}
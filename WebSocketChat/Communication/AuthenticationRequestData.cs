﻿using System.Diagnostics;

namespace TestWebsocketApp.Communication {
    [DebuggerStepThrough]
    public class AuthenticationRequestData : CommObject {
        public override MessageType Type => MessageType.AuthenticationRequest;
        public AuthenticationRequestData() { }
        public AuthenticationRequestData(string username) : this() {
            Username = username;
        }

        public string Username { get; set; }
    }
}
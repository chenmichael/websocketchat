﻿using Newtonsoft.Json;
using System;
using System.Threading;
using System.XConsole;
using TestWebsocketApp.Communication;
using WebSocketSharp;

namespace TestWebsocketApp.Client {
    public class Client {
        private readonly WebSocket client;
        public bool AuthenticationPending { get; private set; } = true;
        public string Username { get; private set; }
        public Client(string url, string username) {
            Username = username;
            client = new WebSocket(url);
            client.OnMessage += Client_OnMessage;

            // Initiate connection
            Console.WriteLine("Connecting to server...");
            client.Connect();
            if (client.IsAlive && client.ReadyState == WebSocketState.Open)
                Console.WriteLine("Client connected to {0}!", client.Url);
            else
                throw new Exception($"Connection to {client.Url} failed!");

            // Attempt authentication
            Console.WriteLine("Authenticating...");
            SendPayload(new AuthenticationRequestData(username));
        }

        private void SendPayload(CommObject payload) {
            client.Send(new MessageDataWrapper(payload, payload.Type).ToString());
        }

        private void Client_OnMessage(object sender, MessageEventArgs e) {
            if (e.IsText)
                HandleMessage(e.Data);
        }

        private void HandleMessage(string data) {
            MessageDataWrapper message;
            try {
                message = JsonConvert.DeserializeObject<MessageDataWrapper>(data);
            } catch (Exception) {
                XConsole.WriteLine(ConsoleColor.Yellow, "Ignoring invalid data: '{0}'", data);
                return;
            }
            switch (message.MessageType) {
            case MessageType.AuthenticationAnswer:
                AuthenticationAnswerData auth;
                try {
                    auth = JsonConvert.DeserializeObject<AuthenticationAnswerData>(message.Payload);
                } catch (Exception) {
                    XConsole.WriteLine(ConsoleColor.Red, "Bad authentication answer!");
                    break;
                }
                if (auth is null) {
                    XConsole.WriteLine(ConsoleColor.Red, "Empty authentication answer!");
                    break;
                }
                FinalizeAuthentication(auth);
                break;
            case MessageType.TextMessageReceive:
                TextMessageReceiveData received;
                try {
                    received = JsonConvert.DeserializeObject<TextMessageReceiveData>(message.Payload);
                } catch (Exception) {
                    XConsole.WriteLine(ConsoleColor.Red, "Bad text message received!");
                    break;
                }
                if (received is null) {
                    XConsole.WriteLine(ConsoleColor.Red, "Empty text message data!");
                    break;
                }
                Console.WriteLine("{0}: {1}", received.Sender, received.Message);
                break;
            default:
                XConsole.WriteLine(ConsoleColor.Red, "Server cannot yet process request of type '{0:x}:{1}'!", ((int)message.MessageType).ToString(), message.MessageType.ToString());
                break;
            }
        }

        private void FinalizeAuthentication(AuthenticationAnswerData auth) {
            if (auth.Success)
                AuthenticationPending = false;
            else {
                try {
                    Disconnect();
                } finally {
                    throw new Exception($"Authentication failed: {auth.Reason}");
                }
            }
            XConsole.WriteLine(ConsoleColor.Green, "Successfully authenticated as {0}!", Username);
        }

        ~Client() {
            Disconnect();
        }

        private void Disconnect() {
            Console.WriteLine("Disconnecting client...");
            client.Close();
            Console.WriteLine("Client disconnected!");
        }

        public void Run() {
            while (AuthenticationPending)
                Thread.Sleep(150);

            Console.WriteLine("Type exit to disconnect the client...");
            while (true) {
                Console.Write(">");
                try {
                    var message = Console.ReadLine();
                    if (message.ToLower() == "exit")
                        break;
                    SendPayload(new TextMessageSendData(message));
                } catch (Exception e) {
                    XConsole.WriteLine(ConsoleColor.Red, "Could not send message: {0}", e.ToString());
                }
            }

            Disconnect();
        }
    }
}
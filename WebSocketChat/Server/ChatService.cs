﻿using Newtonsoft.Json;
using System;
using System.XConsole;
using TestWebsocketApp.Communication;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace TestWebsocketApp.Server {
    internal class ChatService : WebSocketBehavior {
        private void WriteSession() {
            Console.Write("[{0}] ", ID);
        }
        protected override void OnClose(CloseEventArgs e) {
            WriteSession();
            if (e.WasClean)
                XConsole.WriteLine(ConsoleColor.Cyan, "Websocket closed cleanly with close code {1}, reason: {0}!", e.Reason, e.Code.ToString());
            else
                XConsole.WriteLine(ConsoleColor.Yellow, "Websocket closed unexpectedly with close code {1}, reason: {0}!", e.Reason, e.Code.ToString());
        }
        protected override void OnError(ErrorEventArgs e) {
            WriteSession();
            XConsole.WriteLine(ConsoleColor.Red, "Error on socket: ", e.Exception.ToString());
        }
        protected override void OnMessage(MessageEventArgs e) {
            WriteSession();
            if (e.IsText) {
                HandleMessage(e.Data);
            }
        }

        private void HandleMessage(string data) {
            MessageDataWrapper message;
            try {
                message = JsonConvert.DeserializeObject<MessageDataWrapper>(data);
            } catch (Exception) {
                XConsole.WriteLine(ConsoleColor.Yellow, "Ignoring invalid data: '{0}'", data);
                return;
            }
            switch (message.MessageType) {
            case MessageType.AuthenticationRequest:
                AuthenticationRequestData auth;
                try {
                    auth = JsonConvert.DeserializeObject<AuthenticationRequestData>(message.Payload);
                } catch (Exception) {
                    XConsole.WriteLine(ConsoleColor.Red, "Bad authentication request!");
                    break;
                }
                if (auth is null) {
                    XConsole.WriteLine(ConsoleColor.Red, "Empty authentication request!");
                    break;
                }
                HandleAuthentication(auth);
                break;
            case MessageType.TextMessageSend:
                TextMessageSendData send;
                try {
                    send = JsonConvert.DeserializeObject<TextMessageSendData>(message.Payload);
                } catch (Exception) {
                    XConsole.WriteLine(ConsoleColor.Red, "Bad message send request!");
                    break;
                }
                if (send is null || send.Message is null) {
                    XConsole.WriteLine(ConsoleColor.Red, "Empty send request!");
                    break;
                }
                ForwardMessage(send);
                break;
            default:
                XConsole.WriteLine(ConsoleColor.Red, "Server cannot yet process request of type '{0}:{1}'!", ((int)message.MessageType).ToString(), message.MessageType.ToString());
                break;
            }
        }

        private void ForwardMessage(TextMessageSendData send) {
            string sender;
            try {
                sender = Server.GetUsername(ID);
            } catch (Exception) {
                XConsole.WriteLine(ConsoleColor.Red, "Forbidden: Client is not authenticated!");
                return;
            }
            foreach (var receiver in Sessions.IDs) {
                // Dont send to self
                if (receiver == ID)
                    continue;
                SendPayload(receiver, new TextMessageReceiveData(sender, send.Message));
            }
        }

        private void HandleAuthentication(AuthenticationRequestData auth) {
            Console.WriteLine("Requesting authentication with username {0}...", auth.Username);
            var (success, reason) = Server.AuthenticateUser(auth, ID);
            if (success)
                XConsole.WriteLine(ConsoleColor.Green, "User '{0}' successfully authenticated!", auth.Username);
            else
                XConsole.WriteLine(ConsoleColor.Red, "Authentication failed: {0}", reason);
            SendPayload(ID, new AuthenticationAnswerData(success, reason));
        }

        private void SendPayload(string id, CommObject payload) {
            Sessions.SendTo(new MessageDataWrapper(payload, payload.Type).ToString(), id);
        }

        protected override void OnOpen() {
            WriteSession();
            XConsole.WriteLine(ConsoleColor.Green, "Websocket opened!");
        }
    }
}
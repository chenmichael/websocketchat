﻿using System;
using System.Collections.Generic;
using System.Threading;
using TestWebsocketApp.Communication;
using WebSocketSharp.Server;

namespace TestWebsocketApp.Server {
    public class Server {
        /// <summary>
        /// Dictionary of logged in Users and their session IDs.
        /// </summary>
        private static readonly Dictionary<string, string> Users = new Dictionary<string, string>();
        private const string ChatPath = "/chat";
        private readonly WebSocketServer server;
        private bool cancellationpending = false;

        public Server(int port) {
            server = new WebSocketServer(port, false);
            server.AddWebSocketService<ChatService>(ChatPath);
            Console.WriteLine("Starting server...");
            server.Start();
            Console.WriteLine("Server started on port {0}!", port);
        }

        public static (bool success, string reason) AuthenticateUser(AuthenticationRequestData auth, string id) {
            try {
                AddUser(auth.Username, id);
                return (true, null);
            } catch (Exception e) {
                return (false, e.Message);
            }
        }

        private static void AddUser(string username, string id) {
            if (Users.ContainsValue(username))
                throw new InvalidOperationException($"User '{username}' is already logged in!");
            else
                Users.Add(id, username);
        }

        public static string GetUsername(string id) {
            return Users[id];
        }

        ~Server() {
            Stop();
        }

        private void Stop() {
            Console.WriteLine("Stopping server...");
            server.Stop();
            Console.WriteLine("Server stopped!");
        }

        public void Run() {
            Console.CancelKeyPress += Console_CancelKeyPress;
            Console.WriteLine("Press Ctrl + C or UNTBR to stop the server...");
            while (true) {
                if (cancellationpending)
                    break;
                Thread.Sleep(100);
            }

            Stop();
        }

        private void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e) {
            e.Cancel = true;
            cancellationpending = true;
        }
    }
}